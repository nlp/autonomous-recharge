# Auto Recharge - Czech dialogs

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

This is just an extension of original `autonomous-recharge` application
from Softbank Robotics.

## Installation

* you need to have the `autonomous-recharge` application installed
* copy `/home/nao/.local/share/PackageManager/apps/autonomous-recharge` to
  new place
* copy files from this project over the files from `autonomous-recharge`
* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the new package over the previous version

