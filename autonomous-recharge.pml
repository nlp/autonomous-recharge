<?xml version="1.0" encoding="UTF-8" ?>
<Package name="autonomous-recharge" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="interactive/go-to-station" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="interactive/handle-disconnection" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="interactive/leave-station-period" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="interactive/leave-station" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="no-nature/go-to-station-launcher" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="no-nature/warning-mode" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/LastOne" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/TryAgain/tryagain_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/TryAgain/tryagain_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/TryAgain/tryagain_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/LeaveSuccess/leavesuccedded_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/LeaveSuccess/leavesuccedded_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/LeaveSuccess/leavesuccedded_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/TurnDuringScanLeft" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingFailure/movingfailure_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingFailure/movingfailure_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/TurnDuringScanRight" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingSuccess/movingsuccess_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingSuccess/movingsuccess_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingSuccess/movingsuccess_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingTowards/movingtowards_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingTowards/movingtowards_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/MovingTowards/movingtowards_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartGoToStation" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartMoving/startmoving_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartMoving/startmoving_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartMoving/startmoving_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/BatteryWarning" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartSearching/startsearching_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartSearching/startsearching_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StartSearching/startsearching_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/Docking/turning_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/Docking/turning_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationFound/stationfound_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationFound/stationfound_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationFound/stationfound_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/DockingFailure/dockingfailure_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/DockingFailure/dockingfailure_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/DockingFailure/dockingfailure_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationNotFound/stationnotfound_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationNotFound/stationnotfound_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationNotFound/stationnotfound_3" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/DockingSuccess/dockingsuccess" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StationNotFoundLast" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/Idle/littleyes_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/Idle/MoveFingers" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/Idle/cutemove_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/Idle/cutemove_2" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Pepper/Stand/StayBack" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="collaborative_dialog_recharge" src="dialogs/collaborative_dialog_recharge.dlg"/>
        <Dialog name="collaborative_dialog_warning" src="dialogs/collaborative_dialog_warning.dlg"/>
        <Dialog name="interactive_dialog_charging" src="dialogs/interactive_dialog_charging.dlg"/>
        <Dialog name="interactive_dialog_entrance" src="dialogs/interactive_dialog_entrance.dlg"/>
        <Dialog name="interactive_dialog_gotostation" src="dialogs/interactive_dialog_gotostation.dlg"/>
    </Dialogs>
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="base.css" src="html/css/base.css"/>
        <File name="alert20x20.png" src="html/css/img/alert20x20.png"/>
        <File name="app.png" src="html/img/menu/app.png"/>
        <File name="button_off.png" src="html/img/page/button_off.png"/>
        <File name="button_on.png" src="html/img/page/button_on.png"/>
        <File name="warning.png" src="html/img/page/warning.png"/>
        <File name="module.js" src="html/js/core/module.js"/>
        <File name="base.js" src="html/js/core/controllers/base.js"/>
        <File name="states.js" src="html/js/states.js"/>
        <File name="Chinese.json" src="html/lang/Chinese.json"/>
        <File name="English.json" src="html/lang/English.json"/>
        <File name="French.json" src="html/lang/French.json"/>
        <File name="Japanese.json" src="html/lang/Japanese.json"/>
        <File name="warning.html" src="html/no-nature/warning-mode/warning.html"/>
        <File name="din_blackalternate-webfont.eot" src="html/no-nature/warning-mode/font/din_blackalternate-webfont.eot"/>
        <File name="din_blackalternate-webfont.svg" src="html/no-nature/warning-mode/font/din_blackalternate-webfont.svg"/>
        <File name="din_blackalternate-webfont.ttf" src="html/no-nature/warning-mode/font/din_blackalternate-webfont.ttf"/>
        <File name="din_blackalternate-webfont.woff" src="html/no-nature/warning-mode/font/din_blackalternate-webfont.woff"/>
        <File name="din_blackt-webfont.eot" src="html/no-nature/warning-mode/font/din_blackt-webfont.eot"/>
        <File name="din_blackt-webfont.svg" src="html/no-nature/warning-mode/font/din_blackt-webfont.svg"/>
        <File name="din_blackt-webfont.ttf" src="html/no-nature/warning-mode/font/din_blackt-webfont.ttf"/>
        <File name="din_blackt-webfont.woff" src="html/no-nature/warning-mode/font/din_blackt-webfont.woff"/>
        <File name="din_bold-webfont.eot" src="html/no-nature/warning-mode/font/din_bold-webfont.eot"/>
        <File name="din_bold-webfont.svg" src="html/no-nature/warning-mode/font/din_bold-webfont.svg"/>
        <File name="din_bold-webfont.ttf" src="html/no-nature/warning-mode/font/din_bold-webfont.ttf"/>
        <File name="din_bold-webfont.woff" src="html/no-nature/warning-mode/font/din_bold-webfont.woff"/>
        <File name="din_light-webfont.eot" src="html/no-nature/warning-mode/font/din_light-webfont.eot"/>
        <File name="din_light-webfont.svg" src="html/no-nature/warning-mode/font/din_light-webfont.svg"/>
        <File name="din_light-webfont.ttf" src="html/no-nature/warning-mode/font/din_light-webfont.ttf"/>
        <File name="din_light-webfont.woff" src="html/no-nature/warning-mode/font/din_light-webfont.woff"/>
        <File name="din_lightalternate-webfont.eot" src="html/no-nature/warning-mode/font/din_lightalternate-webfont.eot"/>
        <File name="din_lightalternate-webfont.svg" src="html/no-nature/warning-mode/font/din_lightalternate-webfont.svg"/>
        <File name="din_lightalternate-webfont.ttf" src="html/no-nature/warning-mode/font/din_lightalternate-webfont.ttf"/>
        <File name="din_lightalternate-webfont.woff" src="html/no-nature/warning-mode/font/din_lightalternate-webfont.woff"/>
        <File name="din_regular-webfont.eot" src="html/no-nature/warning-mode/font/din_regular-webfont.eot"/>
        <File name="din_regular-webfont.svg" src="html/no-nature/warning-mode/font/din_regular-webfont.svg"/>
        <File name="din_regular-webfont.ttf" src="html/no-nature/warning-mode/font/din_regular-webfont.ttf"/>
        <File name="din_regular-webfont.woff" src="html/no-nature/warning-mode/font/din_regular-webfont.woff"/>
        <File name="stylesheet.css" src="html/no-nature/warning-mode/font/stylesheet.css"/>
        <File name="bg.png" src="html/no-nature/warning-mode/img/bg.png"/>
        <File name="base.html" src="html/partials/base.html"/>
        <File name="animated_recharge.py" src="lib/animated_recharge.py"/>
        <File name="animated_recharge.pyc" src="lib/animated_recharge.pyc"/>
        <File name="ccanimationmanager_recharge.py" src="lib/ccanimationmanager_recharge.py"/>
        <File name="ccanimationmanager_recharge.pyc" src="lib/ccanimationmanager_recharge.pyc"/>
        <File name="cctools_recharge.py" src="lib/cctools_recharge.py"/>
        <File name="cctools_recharge.pyc" src="lib/cctools_recharge.pyc"/>
        <File name="event_helper.py" src="lib/event_helper.py"/>
        <File name="event_helper.pyc" src="lib/event_helper.pyc"/>
        <File name="preferences_checker.py" src="lib/preferences_checker.py"/>
        <File name="preferences_checker.pyc" src="lib/preferences_checker.pyc"/>
        <File name="autonomousLaunchpadPlugin.xml" src="no-nature/go-to-station-launcher/autonomousLaunchpadPlugin.xml"/>
        <File name="bidi_battery_connected.ogg" src="sounds/bidi_battery_connected.ogg"/>
        <File name="bidi_battery_full_01.ogg" src="sounds/bidi_battery_full_01.ogg"/>
        <File name="bidi_battery_full_02.ogg" src="sounds/bidi_battery_full_02.ogg"/>
        <File name="bidi_battery_not_connected.ogg" src="sounds/bidi_battery_not_connected.ogg"/>
        <File name="bidi_need_battery.ogg" src="sounds/bidi_need_battery.ogg"/>
        <File name="bidi_station_detected_01.ogg" src="sounds/bidi_station_detected_01.ogg"/>
        <File name="enu_ono_ba_false_detection_03.ogg" src="sounds/enu_ono_ba_false_detection_03.ogg"/>
        <File name="enu_ono_ba_false_detection_10.ogg" src="sounds/enu_ono_ba_false_detection_10.ogg"/>
        <File name="enu_ono_ba_false_detection_11.ogg" src="sounds/enu_ono_ba_false_detection_11.ogg"/>
        <File name="enu_ono_ba_human_touch_hand_03.ogg" src="sounds/enu_ono_ba_human_touch_hand_03.ogg"/>
        <File name="enu_ono_ba_human_touch_hand_04.ogg" src="sounds/enu_ono_ba_human_touch_hand_04.ogg"/>
        <File name="enu_ono_comprehension_27.ogg" src="sounds/enu_ono_comprehension_27.ogg"/>
        <File name="enu_ono_comptefindshort.ogg" src="sounds/enu_ono_comptefindshort.ogg"/>
        <File name="enu_ono_exclamation_curious_01.ogg" src="sounds/enu_ono_exclamation_curious_01.ogg"/>
        <File name="enu_ono_exclamation_curious_04.ogg" src="sounds/enu_ono_exclamation_curious_04.ogg"/>
        <File name="enu_ono_hesitation_long_01.ogg" src="sounds/enu_ono_hesitation_long_01.ogg"/>
        <File name="enu_ono_hesitation_short_01.ogg" src="sounds/enu_ono_hesitation_short_01.ogg"/>
        <File name="enu_ono_hesitation_short_02.ogg" src="sounds/enu_ono_hesitation_short_02.ogg"/>
        <File name="enu_ono_hesitation_short_03.ogg" src="sounds/enu_ono_hesitation_short_03.ogg"/>
        <File name="enu_ono_hesitation_short_04.ogg" src="sounds/enu_ono_hesitation_short_04.ogg"/>
        <File name="enu_ono_hesitation_short_05.ogg" src="sounds/enu_ono_hesitation_short_05.ogg"/>
    </Resources>
    <Topics>
        <Topic name="collaborative_dialog_recharge_enu" src="dialogs/collaborative_dialog_recharge_enu.top"  topicName="collaborative_dialog_recharge" language="en_US"/>
        <Topic name="collaborative_dialog_recharge_frf" src="dialogs/collaborative_dialog_recharge_frf.top"  topicName="collaborative_dialog_recharge" language="fr_FR"/>
        <Topic name="collaborative_dialog_recharge_jpj" src="dialogs/collaborative_dialog_recharge_jpj.top"  topicName="collaborative_dialog_recharge" language="ja_JP"/>
        <Topic name="collaborative_dialog_recharge_czc" src="dialogs/collaborative_dialog_recharge_czc.top"  topicName="collaborative_dialog_recharge" language="cs_CZ"/>
        <Topic name="collaborative_dialog_warning_enu" src="dialogs/collaborative_dialog_warning_enu.top"  topicName="collaborative_dialog_warning" language="en_US"/>
        <Topic name="collaborative_dialog_warning_frf" src="dialogs/collaborative_dialog_warning_frf.top"  topicName="collaborative_dialog_warning" language="fr_FR"/>
        <Topic name="collaborative_dialog_warning_jpj" src="dialogs/collaborative_dialog_warning_jpj.top"  topicName="collaborative_dialog_warning" language="ja_JP"/>
        <Topic name="collaborative_dialog_warning_czc" src="dialogs/collaborative_dialog_warning_czc.top"  topicName="collaborative_dialog_warning" language="cs_CZ"/>
        <Topic name="interactive_dialog_charging_enu" src="dialogs/interactive_dialog_charging_enu.top"  topicName="interactive_dialog_charging" language="en_US"/>
        <Topic name="interactive_dialog_charging_frf" src="dialogs/interactive_dialog_charging_frf.top"  topicName="interactive_dialog_charging" language="fr_FR"/>
        <Topic name="interactive_dialog_charging_jpj" src="dialogs/interactive_dialog_charging_jpj.top"  topicName="interactive_dialog_charging" language="ja_JP"/>
        <Topic name="interactive_dialog_charging_czc" src="dialogs/interactive_dialog_charging_czc.top"  topicName="interactive_dialog_charging" language="cs_CZ"/>
        <Topic name="interactive_dialog_entrance_enu" src="dialogs/interactive_dialog_entrance_enu.top"  topicName="interactive_dialog_entrance" language="en_US"/>
        <Topic name="interactive_dialog_entrance_frf" src="dialogs/interactive_dialog_entrance_frf.top"  topicName="interactive_dialog_entrance" language="fr_FR"/>
        <Topic name="interactive_dialog_entrance_jpj" src="dialogs/interactive_dialog_entrance_jpj.top"  topicName="interactive_dialog_entrance" language="ja_JP"/>
        <Topic name="interactive_dialog_entrance_czc" src="dialogs/interactive_dialog_entrance_czc.top"  topicName="interactive_dialog_entrance" language="cs_CZ"/>
        <Topic name="interactive_dialog_gotostation_enu" src="dialogs/interactive_dialog_gotostation_enu.top"  topicName="interactive_dialog_gotostation" language="en_US"/>
        <Topic name="interactive_dialog_gotostation_frf" src="dialogs/interactive_dialog_gotostation_frf.top"  topicName="interactive_dialog_gotostation" language="fr_FR"/>
        <Topic name="interactive_dialog_gotostation_jpj" src="dialogs/interactive_dialog_gotostation_jpj.top"  topicName="interactive_dialog_gotostation" language="ja_JP"/>
        <Topic name="interactive_dialog_gotostation_czc" src="dialogs/interactive_dialog_gotostation_czc.top"  topicName="interactive_dialog_gotostation" language="cs_CZ"/>
    </Topics>
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
